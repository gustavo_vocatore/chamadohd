2018-04-16 10:04:28,734 INFO  [ContextLoader] - Root WebApplicationContext: initialization started
2018-04-16 10:04:28,822 INFO  [XmlWebApplicationContext] - Refreshing Root WebApplicationContext: startup date [Mon Apr 16 10:04:28 BRT 2018]; root of context hierarchy
2018-04-16 10:04:28,864 INFO  [XmlBeanDefinitionReader] - Loading XML bean definitions from ServletContext resource [/WEB-INF/spring/root-context.xml]
2018-04-16 10:04:28,966 INFO  [ContextLoader] - Root WebApplicationContext: initialization completed in 230 ms
2018-04-16 10:04:29,010 INFO  [DispatcherServlet] - FrameworkServlet 'springContext': initialization started
2018-04-16 10:04:29,014 INFO  [XmlWebApplicationContext] - Refreshing WebApplicationContext for namespace 'springContext-servlet': startup date [Mon Apr 16 10:04:29 BRT 2018]; parent: Root WebApplicationContext
2018-04-16 10:04:29,014 INFO  [XmlBeanDefinitionReader] - Loading XML bean definitions from ServletContext resource [/WEB-INF/spring/servlet-context.xml]
2018-04-16 10:04:29,409 INFO  [AutowiredAnnotationBeanPostProcessor] - JSR-330 'javax.inject.Inject' annotation found and supported for autowiring
2018-04-16 10:04:29,602 INFO  [LogHelper] - HHH000204: Processing PersistenceUnitInfo [
	name: PUProd
	...]
2018-04-16 10:04:29,704 INFO  [Version] - HHH000412: Hibernate Core {4.3.6.Final}
2018-04-16 10:04:29,706 INFO  [Environment] - HHH000206: hibernate.properties not found
2018-04-16 10:04:29,708 INFO  [Environment] - HHH000021: Bytecode provider name : javassist
2018-04-16 10:04:29,943 INFO  [Version] - HCANN000001: Hibernate Commons Annotations {4.0.5.Final}
2018-04-16 10:04:30,115 INFO  [Dialect] - HHH000400: Using dialect: org.hibernate.dialect.Oracle10gDialect
2018-04-16 10:04:30,326 INFO  [ASTQueryTranslatorFactory] - HHH000397: Using ASTQueryTranslatorFactory
2018-04-16 10:04:30,835 INFO  [RequestMappingHandlerMapping] - Mapped "{[/],methods=[GET]}" onto public org.springframework.web.servlet.ModelAndView br.com.tdv.chamadoHd.controllers.HomeController.home(org.springframework.ui.Model)
2018-04-16 10:04:30,836 INFO  [RequestMappingHandlerMapping] - Mapped "{[/logout]}" onto public java.lang.String br.com.tdv.chamadoHd.controllers.HomeController.logout(javax.servlet.http.HttpSession,org.springframework.ui.Model)
2018-04-16 10:04:30,837 INFO  [RequestMappingHandlerMapping] - Mapped "{[/usuario/login/set/{usuario}/{cnpj}/{rota}],methods=[GET]}" onto public java.lang.String br.com.tdv.chamadoHd.controllers.LoginController.login(java.lang.String,java.lang.String,java.lang.String,java.lang.String,javax.servlet.http.HttpSession,javax.servlet.http.HttpServletRequest)
2018-04-16 10:04:30,837 INFO  [RequestMappingHandlerMapping] - Mapped "{[/usuario/login/logout],methods=[GET]}" onto public java.lang.String br.com.tdv.chamadoHd.controllers.LoginController.logout(javax.servlet.http.HttpSession,org.springframework.ui.Model)
2018-04-16 10:04:30,839 INFO  [RequestMappingHandlerMapping] - Mapped "{[/alterar/chamado],methods=[POST]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaAlteraController.alterarChamado(br.com.tdv.chamadoHd.models.Chamado)
2018-04-16 10:04:30,839 INFO  [RequestMappingHandlerMapping] - Mapped "{[/cadastra/chamado],methods=[POST]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaCadastraController.cadastrarChamado(br.com.tdv.chamadoHd.models.Chamado)
2018-04-16 10:04:30,840 INFO  [RequestMappingHandlerMapping] - Mapped "{[/cadastrar],methods=[GET]}" onto public org.springframework.web.servlet.ModelAndView br.com.tdv.chamadoHd.controllers.PlanilhaCadastraController.cadastro()
2018-04-16 10:04:30,841 INFO  [RequestMappingHandlerMapping] - Mapped "{[/consultar],methods=[GET]}" onto public org.springframework.web.servlet.ModelAndView br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.consultarChamados()
2018-04-16 10:04:30,841 INFO  [RequestMappingHandlerMapping] - Mapped "{[/consulta/chamado/{dataInicial}/{dataFinal}/{usuario}],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.consultarChamado(java.lang.String,java.lang.String,java.lang.String)
2018-04-16 10:04:30,841 INFO  [RequestMappingHandlerMapping] - Mapped "{[/alterar/{codigo}],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.ConsultarChamado(java.lang.Long)
2018-04-16 10:04:30,841 INFO  [RequestMappingHandlerMapping] - Mapped "{[/filial],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.todasFiliais()
2018-04-16 10:04:30,842 INFO  [RequestMappingHandlerMapping] - Mapped "{[/modulo],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.todosModulos()
2018-04-16 10:04:30,842 INFO  [RequestMappingHandlerMapping] - Mapped "{[/ajuda],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.todosAtendentes()
2018-04-16 10:04:30,843 INFO  [RequestMappingHandlerMapping] - Mapped "{[/excluir/chamado/{codigo}],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaExcluiController.excluirChamado(java.lang.Long)
2018-04-16 10:04:30,845 INFO  [RequestMappingHandlerMapping] - Mapped "{[/api/login/logout],methods=[GET],produces=[application/json]}" onto public void br.com.tdv.chamadoHd.apis.LoginRestController.logout(javax.servlet.http.HttpSession)
2018-04-16 10:04:31,095 INFO  [RequestMappingHandlerAdapter] - Looking for @ControllerAdvice: WebApplicationContext for namespace 'springContext-servlet': startup date [Mon Apr 16 10:04:29 BRT 2018]; parent: Root WebApplicationContext
2018-04-16 10:04:31,140 INFO  [RequestMappingHandlerAdapter] - Looking for @ControllerAdvice: WebApplicationContext for namespace 'springContext-servlet': startup date [Mon Apr 16 10:04:29 BRT 2018]; parent: Root WebApplicationContext
2018-04-16 10:04:31,186 INFO  [SimpleUrlHandlerMapping] - Mapped URL path [/resources/**] onto handler 'org.springframework.web.servlet.resource.ResourceHttpRequestHandler#0'
2018-04-16 10:04:31,585 WARN  [TemplateRenderer] - HHH000174: Function template anticipated 4 arguments, but 1 arguments encountered
2018-04-16 10:04:31,586 WARN  [TemplateRenderer] - HHH000174: Function template anticipated 4 arguments, but 1 arguments encountered
2018-04-16 10:04:31,632 WARN  [TemplateRenderer] - HHH000174: Function template anticipated 4 arguments, but 1 arguments encountered
2018-04-16 10:04:31,633 WARN  [TemplateRenderer] - HHH000174: Function template anticipated 4 arguments, but 1 arguments encountered
2018-04-16 10:04:31,733 INFO  [DispatcherServlet] - FrameworkServlet 'springContext': initialization completed in 2723 ms
2018-04-16 10:04:32,604 INFO  [AuthorizerInterceptor] - /chamadohd/
2018-04-16 10:04:32,618 INFO  [AuthorizerInterceptor] - /chamadohd/usuario/login/logout
2018-04-16 10:04:33,835 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/css/bootstrap.css
2018-04-16 10:04:33,837 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/css/tdv-common.css
2018-04-16 10:04:33,850 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/angular.min.js
2018-04-16 10:04:33,850 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/jquery.min.1.11.1.js
2018-04-16 10:04:33,863 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/alertify.js-0.3.11/themes/alertify.core.css
2018-04-16 10:04:33,873 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/jquery-ui.js
2018-04-16 10:04:33,874 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/bootstrap.js
2018-04-16 10:04:33,880 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/css/jquery-ui.css
2018-04-16 10:04:33,895 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/css/sticky-footer-navbar.css
2018-04-16 10:04:33,901 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/select2/select2.min.css
2018-04-16 10:04:33,902 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/font-awesome/css/font-awesome.min.css
2018-04-16 10:04:33,904 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/alertify.js-0.3.11/themes/alertify.default.css
2018-04-16 10:04:33,913 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/dataTables/media/css/dataTables.bootstrap.css
2018-04-16 10:04:33,921 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/angular-sanitize.min.js
2018-04-16 10:04:33,922 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/alertify.js
2018-04-16 10:04:33,926 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/ngAlertify.js
2018-04-16 10:04:33,926 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/alertify.min.js
2018-04-16 10:04:33,935 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/ng-mask.min.js
2018-04-16 10:04:33,936 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/ConfigApp.js
2018-04-16 10:04:33,962 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/js/ConfigApp.js
2018-04-16 10:04:33,971 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/alertify.js-0.3.11/lib/alertify.min.js
2018-04-16 10:04:33,981 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/alertify.js-0.3.11/configAlertify.js
2018-04-16 10:04:33,994 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/ie-emulation-modes-warning.js
2018-04-16 10:04:34,003 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/js/tdvshortlogin-1.0.0.js
2018-04-16 10:04:34,015 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/bootstrap.min.js
2018-04-16 10:04:34,038 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/ie10-viewport-bug-workaround.js
2018-04-16 10:04:34,039 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/select2/select2.min.js
2018-04-16 10:04:34,039 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/js/services/UsuarioService.js
2018-04-16 10:04:34,045 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/dataTables/media/js/jquery.dataTables.js
2018-04-16 10:04:34,040 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/select2/configSelect2.js
2018-04-16 10:04:34,045 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/dataTables/media/js/dataTables.bootstrap.js
2018-04-16 10:04:34,065 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/img/logo-tdv.png
2018-04-16 10:04:34,105 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/img/loading.gif
2018-04-16 10:04:34,106 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/dataTables/configDataTable.js
2018-04-16 10:04:34,290 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/img/favicon.png
2018-04-16 10:04:39,117 INFO  [AuthorizerInterceptor] - /chamadohd/usuario/login/set/fgoes/61139432000172/021
2018-04-16 10:04:39,135 INFO  [LoginController] - login( fgoes, 61139432000172, 021 )...
2018-04-16 10:04:39,135 INFO  [UsuarioService] - getUsuario( fgoes, 61139432000172 )...
2018-04-16 10:04:39,136 INFO  [UsuarioDao] - getUsuario(fgoes, 61139432000172)...
2018-04-16 10:04:39,451 INFO  [UsuarioService] - getConnectionDetail()...
2018-04-16 10:04:39,451 INFO  [UsuarioDao] - getConnectionDetail()...
2018-04-16 10:04:39,569 INFO  [AuthorizerInterceptor] - /chamadohd/
2018-04-16 10:04:39,570 INFO  [HomeController] - home()...
2018-04-16 10:04:39,716 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/img/default-avatar.png
2018-04-16 10:04:39,804 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/fonts/glyphicons-halflings-regular.woff
2018-04-16 10:04:58,963 INFO  [XmlWebApplicationContext] - Closing WebApplicationContext for namespace 'springContext-servlet': startup date [Mon Apr 16 10:04:29 BRT 2018]; parent: Root WebApplicationContext
2018-04-16 10:04:58,979 INFO  [XmlWebApplicationContext] - Closing Root WebApplicationContext: startup date [Mon Apr 16 10:04:28 BRT 2018]; root of context hierarchy
2018-04-16 10:09:11,004 INFO  [ContextLoader] - Root WebApplicationContext: initialization started
2018-04-16 10:09:11,093 INFO  [XmlWebApplicationContext] - Refreshing Root WebApplicationContext: startup date [Mon Apr 16 10:09:11 BRT 2018]; root of context hierarchy
2018-04-16 10:09:11,135 INFO  [XmlBeanDefinitionReader] - Loading XML bean definitions from ServletContext resource [/WEB-INF/spring/root-context.xml]
2018-04-16 10:09:11,220 INFO  [ContextLoader] - Root WebApplicationContext: initialization completed in 214 ms
2018-04-16 10:09:11,263 INFO  [DispatcherServlet] - FrameworkServlet 'springContext': initialization started
2018-04-16 10:09:11,266 INFO  [XmlWebApplicationContext] - Refreshing WebApplicationContext for namespace 'springContext-servlet': startup date [Mon Apr 16 10:09:11 BRT 2018]; parent: Root WebApplicationContext
2018-04-16 10:09:11,267 INFO  [XmlBeanDefinitionReader] - Loading XML bean definitions from ServletContext resource [/WEB-INF/spring/servlet-context.xml]
2018-04-16 10:09:11,634 INFO  [AutowiredAnnotationBeanPostProcessor] - JSR-330 'javax.inject.Inject' annotation found and supported for autowiring
2018-04-16 10:09:11,833 INFO  [LogHelper] - HHH000204: Processing PersistenceUnitInfo [
	name: PUProd
	...]
2018-04-16 10:09:11,926 INFO  [Version] - HHH000412: Hibernate Core {4.3.6.Final}
2018-04-16 10:09:11,928 INFO  [Environment] - HHH000206: hibernate.properties not found
2018-04-16 10:09:11,930 INFO  [Environment] - HHH000021: Bytecode provider name : javassist
2018-04-16 10:09:12,167 INFO  [Version] - HCANN000001: Hibernate Commons Annotations {4.0.5.Final}
2018-04-16 10:09:12,335 INFO  [Dialect] - HHH000400: Using dialect: org.hibernate.dialect.Oracle10gDialect
2018-04-16 10:09:12,537 INFO  [ASTQueryTranslatorFactory] - HHH000397: Using ASTQueryTranslatorFactory
2018-04-16 10:09:13,044 INFO  [RequestMappingHandlerMapping] - Mapped "{[/],methods=[GET]}" onto public org.springframework.web.servlet.ModelAndView br.com.tdv.chamadoHd.controllers.HomeController.home(org.springframework.ui.Model)
2018-04-16 10:09:13,045 INFO  [RequestMappingHandlerMapping] - Mapped "{[/logout]}" onto public java.lang.String br.com.tdv.chamadoHd.controllers.HomeController.logout(javax.servlet.http.HttpSession,org.springframework.ui.Model)
2018-04-16 10:09:13,046 INFO  [RequestMappingHandlerMapping] - Mapped "{[/usuario/login/set/{usuario}/{cnpj}/{rota}],methods=[GET]}" onto public java.lang.String br.com.tdv.chamadoHd.controllers.LoginController.login(java.lang.String,java.lang.String,java.lang.String,java.lang.String,javax.servlet.http.HttpSession,javax.servlet.http.HttpServletRequest)
2018-04-16 10:09:13,047 INFO  [RequestMappingHandlerMapping] - Mapped "{[/usuario/login/logout],methods=[GET]}" onto public java.lang.String br.com.tdv.chamadoHd.controllers.LoginController.logout(javax.servlet.http.HttpSession,org.springframework.ui.Model)
2018-04-16 10:09:13,048 INFO  [RequestMappingHandlerMapping] - Mapped "{[/alterar/chamado],methods=[POST]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaAlteraController.alterarChamado(br.com.tdv.chamadoHd.models.Chamado)
2018-04-16 10:09:13,049 INFO  [RequestMappingHandlerMapping] - Mapped "{[/cadastra/chamado],methods=[POST]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaCadastraController.cadastrarChamado(br.com.tdv.chamadoHd.models.Chamado)
2018-04-16 10:09:13,049 INFO  [RequestMappingHandlerMapping] - Mapped "{[/cadastrar],methods=[GET]}" onto public org.springframework.web.servlet.ModelAndView br.com.tdv.chamadoHd.controllers.PlanilhaCadastraController.cadastro()
2018-04-16 10:09:13,051 INFO  [RequestMappingHandlerMapping] - Mapped "{[/consultar],methods=[GET]}" onto public org.springframework.web.servlet.ModelAndView br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.consultarChamados()
2018-04-16 10:09:13,051 INFO  [RequestMappingHandlerMapping] - Mapped "{[/consulta/chamado/{dataInicial}/{dataFinal}/{usuario}],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.consultarChamado(java.lang.String,java.lang.String,java.lang.String)
2018-04-16 10:09:13,051 INFO  [RequestMappingHandlerMapping] - Mapped "{[/alterar/{codigo}],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.ConsultarChamado(java.lang.Long)
2018-04-16 10:09:13,052 INFO  [RequestMappingHandlerMapping] - Mapped "{[/filial],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.todasFiliais()
2018-04-16 10:09:13,052 INFO  [RequestMappingHandlerMapping] - Mapped "{[/modulo],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.todosModulos()
2018-04-16 10:09:13,053 INFO  [RequestMappingHandlerMapping] - Mapped "{[/ajuda],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaConsultaController.todosAtendentes()
2018-04-16 10:09:13,054 INFO  [RequestMappingHandlerMapping] - Mapped "{[/excluir/chamado/{codigo}],methods=[GET]}" onto public org.springframework.http.ResponseEntity<java.lang.String> br.com.tdv.chamadoHd.controllers.PlanilhaExcluiController.excluirChamado(java.lang.Long)
2018-04-16 10:09:13,056 INFO  [RequestMappingHandlerMapping] - Mapped "{[/api/login/logout],methods=[GET],produces=[application/json]}" onto public void br.com.tdv.chamadoHd.apis.LoginRestController.logout(javax.servlet.http.HttpSession)
2018-04-16 10:09:13,311 INFO  [RequestMappingHandlerAdapter] - Looking for @ControllerAdvice: WebApplicationContext for namespace 'springContext-servlet': startup date [Mon Apr 16 10:09:11 BRT 2018]; parent: Root WebApplicationContext
2018-04-16 10:09:13,359 INFO  [RequestMappingHandlerAdapter] - Looking for @ControllerAdvice: WebApplicationContext for namespace 'springContext-servlet': startup date [Mon Apr 16 10:09:11 BRT 2018]; parent: Root WebApplicationContext
2018-04-16 10:09:13,399 INFO  [SimpleUrlHandlerMapping] - Mapped URL path [/resources/**] onto handler 'org.springframework.web.servlet.resource.ResourceHttpRequestHandler#0'
2018-04-16 10:09:13,813 WARN  [TemplateRenderer] - HHH000174: Function template anticipated 4 arguments, but 1 arguments encountered
2018-04-16 10:09:13,813 WARN  [TemplateRenderer] - HHH000174: Function template anticipated 4 arguments, but 1 arguments encountered
2018-04-16 10:09:13,858 WARN  [TemplateRenderer] - HHH000174: Function template anticipated 4 arguments, but 1 arguments encountered
2018-04-16 10:09:13,858 WARN  [TemplateRenderer] - HHH000174: Function template anticipated 4 arguments, but 1 arguments encountered
2018-04-16 10:09:13,948 INFO  [DispatcherServlet] - FrameworkServlet 'springContext': initialization completed in 2685 ms
2018-04-16 10:09:14,493 INFO  [AuthorizerInterceptor] - /chamadohd/
2018-04-16 10:09:14,523 INFO  [AuthorizerInterceptor] - /chamadohd/usuario/login/logout
2018-04-16 10:09:15,705 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/css/tdv-common.css
2018-04-16 10:09:15,706 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/css/bootstrap.css
2018-04-16 10:09:15,711 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/jquery.min.1.11.1.js
2018-04-16 10:09:15,712 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/bootstrap.js
2018-04-16 10:09:15,715 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/angular.min.js
2018-04-16 10:09:15,717 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/jquery-ui.js
2018-04-16 10:09:15,721 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/alertify.js-0.3.11/themes/alertify.core.css
2018-04-16 10:09:15,722 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/css/jquery-ui.css
2018-04-16 10:09:15,740 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/select2/select2.min.css
2018-04-16 10:09:15,741 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/font-awesome/css/font-awesome.min.css
2018-04-16 10:09:15,743 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/alertify.js-0.3.11/themes/alertify.default.css
2018-04-16 10:09:15,745 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/dataTables/media/css/dataTables.bootstrap.css
2018-04-16 10:09:15,746 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/angular-sanitize.min.js
2018-04-16 10:09:15,748 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/css/sticky-footer-navbar.css
2018-04-16 10:09:15,748 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/alertify.js
2018-04-16 10:09:15,750 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/alertify.min.js
2018-04-16 10:09:15,753 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/ng-mask.min.js
2018-04-16 10:09:15,755 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/ConfigApp.js
2018-04-16 10:09:15,756 INFO  [AuthorizerInterceptor] - /chamadohd/resources/angular/ngAlertify.js
2018-04-16 10:09:15,761 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/js/ConfigApp.js
2018-04-16 10:09:15,766 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/alertify.js-0.3.11/lib/alertify.min.js
2018-04-16 10:09:15,771 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/alertify.js-0.3.11/configAlertify.js
2018-04-16 10:09:15,775 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/ie-emulation-modes-warning.js
2018-04-16 10:09:15,790 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/js/tdvshortlogin-1.0.0.js
2018-04-16 10:09:15,795 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/bootstrap.min.js
2018-04-16 10:09:15,800 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/js/ie10-viewport-bug-workaround.js
2018-04-16 10:09:15,805 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/js/services/UsuarioService.js
2018-04-16 10:09:15,810 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/select2/select2.min.js
2018-04-16 10:09:15,815 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/select2/configSelect2.js
2018-04-16 10:09:15,820 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/dataTables/media/js/jquery.dataTables.js
2018-04-16 10:09:15,823 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/dataTables/media/js/dataTables.bootstrap.js
2018-04-16 10:09:15,824 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/img/logo-tdv.png
2018-04-16 10:09:15,824 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/img/loading.gif
2018-04-16 10:09:15,827 INFO  [AuthorizerInterceptor] - /chamadohd/resources/plugins/dataTables/configDataTable.js
2018-04-16 10:09:19,332 INFO  [AuthorizerInterceptor] - /chamadohd/usuario/login/set/fgoes/61139432000172/021
2018-04-16 10:09:19,349 INFO  [LoginController] - login( fgoes, 61139432000172, 021 )...
2018-04-16 10:09:19,349 INFO  [UsuarioService] - getUsuario( fgoes, 61139432000172 )...
2018-04-16 10:09:19,349 INFO  [UsuarioDao] - getUsuario(fgoes, 61139432000172)...
2018-04-16 10:09:19,690 INFO  [UsuarioService] - getConnectionDetail()...
2018-04-16 10:09:19,691 INFO  [UsuarioDao] - getConnectionDetail()...
2018-04-16 10:09:19,823 INFO  [AuthorizerInterceptor] - /chamadohd/
2018-04-16 10:09:19,824 INFO  [HomeController] - home()...
2018-04-16 10:09:19,942 INFO  [AuthorizerInterceptor] - /chamadohd/resources/core/img/default-avatar.png
2018-04-16 10:09:19,970 INFO  [AuthorizerInterceptor] - /chamadohd/resources/bootstrap/fonts/glyphicons-halflings-regular.woff
2018-04-16 10:09:21,026 INFO  [AuthorizerInterceptor] - /chamadohd/cadastrar
2018-04-16 10:09:21,163 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/util/ValidaValores.js
2018-04-16 10:09:21,178 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/util/FunctionSpinner.js
2018-04-16 10:09:21,185 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/util/DataService.js
2018-04-16 10:09:21,190 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/services/ChamadoService.js
2018-04-16 10:09:21,195 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/services/FilialService.js
2018-04-16 10:09:21,206 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/services/ModuloService.js
2018-04-16 10:09:21,207 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/controllers/ChamadoController.js
2018-04-16 10:09:21,207 INFO  [AuthorizerInterceptor] - /chamadohd/resources/app/services/AjudaService.js
2018-04-16 10:09:21,291 INFO  [AuthorizerInterceptor] - /chamadohd/filial
2018-04-16 10:09:21,291 INFO  [AuthorizerInterceptor] - /chamadohd/modulo
2018-04-16 10:09:21,293 INFO  [AuthorizerInterceptor] - /chamadohd/ajuda
2018-04-16 10:09:34,154 INFO  [XmlWebApplicationContext] - Closing WebApplicationContext for namespace 'springContext-servlet': startup date [Mon Apr 16 10:09:11 BRT 2018]; parent: Root WebApplicationContext
2018-04-16 10:09:34,160 INFO  [XmlWebApplicationContext] - Closing Root WebApplicationContext: startup date [Mon Apr 16 10:09:11 BRT 2018]; root of context hierarchy
