# TDV Spring Boot Archetype

### Ajustes após criar um novo Maven Project baseado neste Archetype   

todos os packages devem ter o prefixo br.com.tdv.NOME_DA_APLICACAO exemplos:
br.com.tdv.coleta.controllers
br.com.tdv.coleta.services
br.com.tdv.coleta.dados
br.com.tdv.coleta.filters
br.com.tdv.coleta.builders
br.com.tdv.coleta.helpers

Modificar o arquivo: src/main/webapp/resources/core/js/ConfigApp.js
 
```
	var _server = function(url) {
		return "/NOME_DA_APLICACAO" + url;
	};
```   

Obs..:    
NOME_DA_APLICACAO deve ser em minusculo, como deve aparecer no navegador
exemplo se o nome da aplicação fosse "coleta": 
http://localhost:7001/coleta/    
```
	var _server = function(url) {
		return "/coleta" + url;
	};
```   
------------------------------------------------------------------------    


Modificar o Context root da Aplicação:

nas Propriedades do Projeto > Web Project Settings > Context root
exemplo: coleta

obs: em minusculo

---------------------------------------------------------------------------

Modificar pom.xml
finalName = NOME_DA_APLICACAO
version = VERSAO_DA_APLICACAO

### Para rodar um projeto no tomcat em ambiente de desenvolvimento

* Tomcat
1. Criar a var de ambiente WEB_SERVER com o valor TOMCAT: WEB_SERVER="TOMCAT"
2. Configurar o server.xml do tomcat
- Dentro da tag <GlobalNamingResources> coloque o xml abaixo:
```xml
<Resource auth="Container" closeMethod="close" 
		  driverClassName="oracle.jdbc.driver.OracleDriver" 
		  factory="oracle.jdbc.pool.OracleDataSourceFactory" 
		  global="jdbc/TdvGlobal" maxActive="100" maxIdle="20" 
		  maxWait="20000" minIdle="5" name="jdbc/TdvGlobal" 
		  password="aged12" poolPreparedStatements="true" 
		  removeAbandoned="true" removeAbandonedTimeout="20" 
		  testOnBorrow="true" testOnReturn="false" testWhileIdle="true" 
		  type="oracle.jdbc.pool.OracleDataSource" 
		  url="jdbc:oracle:thin:@192.168.9.101:1526/tdp" 
		  user="tdvadm" validationInterval="20000" 
		  validationQuery="SELECT SYSDATE FROM DUAL"/>
```
3. Configurar o context.xml
 - Dentro da tag <context> adicione o xml abaixo:
```xml
<ResourceLink name="jdbc/TdvGlobal" global="jdbc/TdvGlobal" type="javax.sql.DataSource"/>
```
4. Copiar o jar ojdbc6-11.1.0.jar para a pasta lib do tomcat

------------------------------------------------------------------------------------------------------------------

### Stagemonitor

Para utilizar o stagemonitor em seu browser, basta seguir o tutorial que está no link:

https://github.com/stagemonitor/stagemonitor/wiki/Only-show-the-widget-for-authorized-users

[Stagemonitor](https://github.com/stagemonitor/stagemonitor/wiki/Only-show-the-widget-for-authorized-users)


Não esqueça de verificar o nome da aplicação no arquivo stagemonitor.properties localizado na pasta src/main/resources.	

-------------------------------------------------------------------------------------------------------------------
	
### Exemplos de Mapeamento JPA

**Foi criado uma modelagem para servir de exemplo de como maperar as classes de modelo com JPA**
Pessoa --| possui N Telefones
Pessoa --| possui N Cnpjs
Cnpj-----| pertence a N pessoas

+--------+0.*          1.*+----------+           
| Pessoa |----------------| Telefone |
+--------+			      +----------+
    |0.*
    |                     +------------+             1.*+------+
    +---------------------| PessoaCnpj |----------------| Cnpj |
                          +------------+                +------+

### Características do Archetype 

O Archetype já vem preparado para trabalhar com as seguintes tecnologias:
* Spring Boot 1.5.8
* Content Negotiation
* Cache
* JPA
* Spring MVC
* Exception Handler
* Stage Monitor
* JMS

# OBS: As classes e pacotes que não forem utilizadas devem ser removidas do projeto.

# OBS2: Não esqueça de configurar a váriavel de ambiente TDV_DATASOURCE.

 

	
	