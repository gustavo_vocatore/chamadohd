package br.com.tdv.teste.jms;

import org.springframework.stereotype.Component;

import br.com.tdv.teste.models.JMSProdutor;

@Component
public class GerenciadorJMS {
	
	private String message;	
	private JMSProdutor produtor;
	
	@SuppressWarnings("unused")
	private GerenciadorJMS() {}
	
	public GerenciadorJMS(String message, JMSProdutor produtor) {
		this.message = message;
		this.produtor = produtor;
	}
	
	public void send(){
		this.produtor.send(this.message);
	}
}
