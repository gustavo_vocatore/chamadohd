package br.com.tdv.teste.models;

import java.io.Serializable;
import java.util.Arrays;

public class Email implements Serializable {

	private static final long serialVersionUID = 4351714406800291770L;

	private String from;
	private String to;
	private String[] cc;
	private String subject;
	private String body;
	private EmailAttachment[] attachments;

	public Email() {
	}

	public Email(String from, String to, String subject, String body) {
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.body = body;
	}

	public Email(String from, String to, String body, String subject, String[] cc, EmailAttachment[] attachments) {
		this(from, to, subject, body);
		this.cc = cc;
		this.attachments = attachments;
	}

	public Email(String from, String to, String body, String subject, EmailAttachment[] attachments) {
		this(from, to, subject, body);
		this.attachments = attachments;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public EmailAttachment[] getAttachments() {
		return attachments;
	}

	public void setAttachments(EmailAttachment[] attachments) {
		this.attachments = attachments;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String[] getCc() {
		return cc;
	}

	public void setCc(String[] cc) {
		this.cc = cc;
	}

	@Override
	public String toString() {
		return "Email [from=" + from + ", to=" + to + ", cc=" + Arrays.toString(cc) + ", subject=" + subject + ", body="
				+ body + "]";
	}

}
