package br.com.tdv.teste.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.tdv.teste.models.Pessoa;

@Controller
public class TesteController {

	@RequestMapping(path="/cadastro", method=RequestMethod.GET)
	public ModelAndView teste() {
		ModelAndView mv = new ModelAndView("index");
		mv.addObject("content_page", "./teste/cadastro.jsp");
		return mv;
	}	
	
	@RequestMapping(value="/cadastra/pessoa", method=RequestMethod.POST)
	public ResponseEntity<String> cadastrarPessoa(@RequestBody Pessoa pessoa) {
			try{   
				return new ResponseEntity<String>(new ObjectMapper().writeValueAsString("cadastrado"), HttpStatus.OK);
			}catch(Exception e){
				e.printStackTrace();
				return new ResponseEntity<String>("erro", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}