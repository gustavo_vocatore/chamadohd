package br.com.tdv.teste.controllers;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.tdv.teste.services.UsuarioService;
import br.com.tdvframework.beans.UsuarioWebLogado;

@Controller
@RequestMapping("/usuario/login")
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired 
	private UsuarioService usuarioService;
	
	/**
	 * 
	 * @param usuario
	 * @param cnpj
	 * @param rota
	 * @param session
	 * @return String
	 * @throws SQLException
	 * 
	 */
	
	@GetMapping(path="/set/{usuario}/{cnpj}/{rota}")
	public String login(@PathVariable String usuario, @PathVariable String cnpj, @PathVariable String rota, String next, HttpSession session, HttpServletRequest request){
		logger.info( "login( "+usuario+", "+cnpj+", "+rota+" )..." );
		UsuarioWebLogado usuarioWebLogado = usuarioService.getUsuario(usuario, cnpj);
		session.setAttribute("usuarioLogado", usuarioWebLogado);
		session.setAttribute("connectionDetail", this.usuarioService.getConnectionDetail());
		if(next != null && next != ""){
			return "redirect:" + next;
		}else{
			return "redirect:/";
		}
	}

	/**
	 * 
	 * Destroy Session
	 * @param session
	 * @param model 
	 * @return String
	 * 
	 */
	
	@GetMapping(path="/logout")
	public String logout(HttpSession session, Model model) {
		session.invalidate();
		model.addAttribute("content_page", "core/logout.jsp");
		return "index";
	}	
	
	
}
