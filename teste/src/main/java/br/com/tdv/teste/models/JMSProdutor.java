package br.com.tdv.teste.models;

public interface JMSProdutor {
	
	public abstract void send(String message);

}
