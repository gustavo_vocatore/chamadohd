package br.com.tdv.teste.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
		
	@GetMapping(path="/")
	public ModelAndView home(Model model) {
		logger.info("home()...");		
		ModelAndView mv = new ModelAndView("index");
		mv.addObject("content_page", "index-content.jsp");		
		return mv;
	}
			
	@GetMapping(path="/logout")
	public String logout(HttpSession session, Model model) {
		logger.info("logout()...");
		session.invalidate();
		model.addAttribute("content_page", "core/logout.jsp");
		return "index";
	}	
		
	@GetMapping(path = "httpErrors")
	public String renderErrorPage(HttpServletRequest httpRequest) {
		int httpErrorCode = getErrorCode(httpRequest);
		return "core/"+String.valueOf(httpErrorCode);
	}

	private int getErrorCode(HttpServletRequest httpRequest) {
		return (Integer) httpRequest.getAttribute("javax.servlet.error.status_code");
	}
	
}
