package br.com.tdv.teste.jms;

import org.springframework.jms.annotation.JmsListener;

import br.com.tdv.teste.models.JMSConsumidor;

/**
 * Exemplo de consumidor de JMS 
 *
 */
public class ConsumidorImpl implements JMSConsumidor {

	/**
	 * Exemplo de método de consumo de jms. Método está com a anotação comentada.
	 * 
	 */
	@Override
	@JmsListener(destination="queue.filaASerConsumida", concurrency="1")
	// destination = a fila a ser consumida, concurrency = numero de consumidores que
	// desejo possuir.
	public void receive(String msg) {
		System.out.println(msg);
	}
}
