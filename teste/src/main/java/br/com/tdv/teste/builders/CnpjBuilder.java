package br.com.tdv.teste.builders;

import br.com.tdv.teste.helpers.RandomValueGeneratorHelper;
import br.com.tdv.teste.models.Cnpj;

public class CnpjBuilder {

	private Cnpj cnpj = new Cnpj(null, RandomValueGeneratorHelper.anyIntegerStr());
	
	public Cnpj build(){
		return this.cnpj;
	}
	
	public CnpjBuilder withId(Long id){
		this.cnpj.setId(id);
		return this;
	}
	
	public CnpjBuilder withNumero(String numero){
		this.cnpj.setNumero(numero);
		return this;
	}
	
}
