package br.com.tdv.teste.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.tdv.teste.models.Telefone;



@Repository
public interface TelefoneRepository extends CrudRepository<Telefone, Long> {

}
