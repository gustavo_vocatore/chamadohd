package br.com.tdv.teste.models;

import java.io.Serializable;

public class EmailAttachment implements Serializable {

	private static final long serialVersionUID = -8189206229953249787L;

	//private byte[] attachmentBase64;
	private String attachmentBase64;
	private String filename;
	private String mimeType = "text/plain";

	public EmailAttachment() {
	}

	public EmailAttachment(String attachmentBase64, String filename, String mimeType) {
		this.attachmentBase64 = attachmentBase64;
		this.filename = filename;
		this.mimeType = mimeType;
	}

	public String getAttachmentBase64() {
		return attachmentBase64;
	}

	public void setAttachmentBase64(String attachmentBase64) {
		this.attachmentBase64 = attachmentBase64;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@Override
	public String toString() {
		return "EmailAttachment [attachmentBase64=" + attachmentBase64 + ", filename=" + filename + ", mimeType="
				+ mimeType + "]";
	}

}
