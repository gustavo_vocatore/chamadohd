package br.com.tdv.teste.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.tdv.teste.models.Pessoa;



@Repository
public interface PessoaRepository extends CrudRepository<Pessoa, Long> {

}
