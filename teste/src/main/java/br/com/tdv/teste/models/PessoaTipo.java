package br.com.tdv.teste.models;

public enum PessoaTipo {
	CLIENTE,
	FORNECEDOR,
	EMPRESA,
	FUNCIONARIO,
	MOTORISTA,
	PROPRIETARIO,
	ROTA
}
