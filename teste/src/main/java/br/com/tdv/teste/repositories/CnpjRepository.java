package br.com.tdv.teste.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.tdv.teste.models.Cnpj;


@Repository
public interface CnpjRepository extends CrudRepository<Cnpj, Long> {
	
	public List<Cnpj> findAll();

}
