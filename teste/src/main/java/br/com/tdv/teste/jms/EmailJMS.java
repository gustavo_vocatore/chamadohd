package br.com.tdv.teste.jms;

import javax.jms.JMSException;
import javax.jms.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import br.com.tdv.teste.models.JMSProdutor;

@Async
@Component
public class EmailJMS implements JMSProdutor {

	private static final Logger logger = LoggerFactory.getLogger(EmailJMS.class);
	
	private JmsTemplate jmsTemplate;
	
	@Qualifier("queue.Email")
	private Queue queueEmail;
	
	@Autowired
	public EmailJMS(JmsTemplate jmsTemplate, Queue queueEmail) {
		this.jmsTemplate = jmsTemplate;
		this.queueEmail =  queueEmail;
	}
	
	@Override
	public void send(String message){
		try {
			jmsTemplate.convertAndSend(queueEmail.getQueueName(), message);
		} catch (JmsException | JMSException e) {			
			logger.error(e.getMessage());
		} 
	}
}
