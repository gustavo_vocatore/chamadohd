package br.com.tdv.teste.confs;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@Configuration
@EnableJms
public class JMSConfiguration {
	
	@Bean(name="queue.Email")
	public Queue queueEmail() {
		return new ActiveMQQueue("queue.Email");
	}
}
